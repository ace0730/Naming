import email.header
import datetime
import smtplib
import json
import os
import dataset
import time
import imaplib
import sys
import email
import re

EMAIL_ACCOUNT = "admin@tigerscyberchallenge.net"
EMAIL_FOLDER = "Inbox"
PASS = "S2!ZBCWVFx@GBJ4o"


#connect to the localdatabase
db = dataset.connect('sqlite:////var/www/challengedb.db')
#login every time the while loop starts        
M = imaplib.IMAP4_SSL('imap.gmail.com')


def main():


    #Checks if running, keeps program from running more than once
    pid = str(os.getpid())
    pidfile = "/tmp/mydaemon.pid"

    if os.path.isfile(pidfile):
        print "%s already exists, exiting" % pidfile
        sys.exit()
    else:
        file(pidfile, 'w').write(pid)


    #login to the account
        
    try:
        rv, data = M.login(EMAIL_ACCOUNT, PASS)
    except imaplib.IMAP4.error:
        print "LOGIN FAILED!!! "
        sys.exit(1)

        print rv, data

    while(1):

        try:

            
            #select a mailbox and list all new email
            rv, data = M.select(EMAIL_FOLDER)
            if rv == 'OK':
                print "Processing mailbox...\n"
                process_mailbox(M)
                M.close()
            else:
                print "ERROR: Unable to open mailbox ", rv


            update_website()


        except Exception as e:
            print "Caught an error... restarting!"
	    print e
	    os.unlink(pidfile)
	    print "Exiting"
	    sys.exit(1)

        #sleep for a minute between processing
        print "Sleeping for two minutes"    
        time.sleep(60)



#processes the mailbox and lists all the mail
def process_mailbox(M):

    try:
        found_team = False
        processed_email = False

        rv, data = M.search(None, "ALL")
        if rv != 'OK':
            print "No messages found!"
            return

        for num in data[0].split():
            rv, data = M.fetch(num, '(RFC822)')
            if rv != 'OK':
                print "ERROR getting message", num
                return

            msg = email.message_from_string(data[0][1])
            decode = email.header.decode_header(msg['Subject'])[0]
            subject = unicode(decode[0])
            print "MsgID : " + num
            print "From : " + email.utils.parseaddr(msg['from'])[1]
            email_from = email.utils.parseaddr(msg['from'])[1]
            print 'Subject : %s' % (subject)
            print 'Raw Date:', msg['Date']
            # Now convert to local date-time
            date_tuple = email.utils.parsedate_tz(msg['Date'])
            if date_tuple:
                local_date = datetime.datetime.fromtimestamp(
                    email.utils.mktime_tz(date_tuple))
                print "Local Date:", \
                    local_date.strftime("%a, %d %b %Y %H:%M:%S")

	    if (get_first_text_block(msg)):
                print "Content : " + get_first_text_block(msg)
	    else:
		print "something goofy is up with this email, content could not print, going to try ghetto work around"


	    msg_content = get_first_text_block(msg)
            if (msg_content):
                msg_content = msg_content.splitlines()
            else:
                msg_content = data[0][1]

	    email_from_register = ""
		#get the email address from the content if not registered
	    for line in msg_content:
		if ("Email:" in line) and (email_from == "admin@tigerscyberchallenge.net"):
		    line = line.replace("Email:","",1)
		    line = line.strip()
		    email_from_register = line
		    print "found a new email: " + email_from_register

                if "team" in line.lower() or "team:" in line.lower():

                    line = line.lower().replace("team","",1)
                    line = line.replace(":", "", 1)
                    line = line.strip()
                    team = line.split()[0]
                    print "Found a team name: " + team
                    found_team = True

		if (email_from_register):
	            #check the from address
	            to_process = parse_email_address(email_from_register)
		    print "processing a new email from admin"
	        else:
	            to_process = check_account(email_from)
		    print "processing an email from non-admin user"
            #if a valid email, perform...
            if (to_process):
		print "in to_process!"
                #check if email registered, if not register
                userid = check_account(email_from_register)
		if userid is False and email_from_register:
                    #register the email account
                    #first grab the team name

		    if found_team is True:
			add_account(email_from_register, team)
                        send_email(email_from_register, "You are wrthy!", "You successfully registered with team : " + team + "\n The username/password for the website is user: cyber-tiger pass: 7is" + reg_note + "\n" + "All of the challenges are at http://tigerscyberchallenge.net/challenges.html -- email your answers to this email!")
                        move_email_to_process(num)

                    if found_team is False:        
                        add_account(email_from_register, "noteam")
                        send_email(email_from_register, "You are worthy!", "You did not specify a team so you are registered to noteam! \n The username/password for the website is user: cyber-tiger pass: 7is" + reg_note + "\n" + "All of the challenges are at http://tigerscyberchallenge.net/challenges.html -- email your answers to this email!")
                        move_email_to_process(num)
                #already has a registered account, start processed email
                else:
                    print "parsing for solutions"
                    #solution parser here
		    userid = check_account(email_from)
                    msg_content = get_first_text_block(msg)
                    if (msg_content):
                        msg_content = msg_content.splitlines()
                    else:
                        msg_content = data[0][1]

                    if parse_email_body(msg_content, lvl0key) is True:
                        print "found the solution to lvl0"
                        db['accounts'].update(dict(id=userid, level0="complete"), ['id'])
                        send_email(email_from, "Congrats on level 0", lvl0)
                         #move the email to processed
                        move_email_to_process(num)

                    elif parse_email_body(msg_content, lvl01key) is True:
                        print "found the solution to lvl01"
                        db['accounts'].update(dict(id=userid, level01="complete"), ['id'])
                        send_email(email_from, "Congrats on level 01", lvl01)
                         #move the email to processed
                        move_email_to_process(num)

                    elif parse_email_body(msg_content, lvl02key) is True:
                        print "found the solution to lvl02"
                        db['accounts'].update(dict(id=userid, level02="complete"), ['id'])
                        send_email(email_from, "Congrats on level 02", lvl02)
                         #move the email to processed
                        move_email_to_process(num)

                    elif parse_email_body(msg_content, lvl1key) is True:
                        print "found the solution to lvl1"
			db['accounts'].update(dict(id=userid, level1="complete"), ['id'])
                        send_email(email_from,"Congrats on level 2",  lvl1)    
                        #move the email to processed
                        move_email_to_process(num)                
                    elif parse_email_body(msg_content, lvl2key) is True:
                        print "found the solution to lvl2"
			db['accounts'].update(dict(id=userid, level2="complete"), ['id'])
                        send_email(email_from,"Congrats on level 3",  lvl2)
                        #move the email to processed
                        move_email_to_process(num)
                    elif parse_email_body(msg_content, lvl3key) is True:
                        print "found the solution to lvl3"
			db['accounts'].update(dict(id=userid, level3="complete"), ['id'])
                        send_email(email_from,"Congrats on level 4",  lvl3)
                        #move the email to processed
                        move_email_to_process(num)
                    elif parse_email_body(msg_content, lvl4key) is True:
                        print "found the solution to lvl4"
			db['accounts'].update(dict(id=userid, level4="complete"), ['id'])
                        send_email(email_from,"Congrats on level 4",  lvl4)
			move_email_to_process(num)

                    elif parse_email_body(msg_content, lvl5key) is True:
                        print "found the solution to lvl5"
                        db['accounts'].update(dict(id=userid, level5="complete"), ['id'])
                        send_email(email_from, "Congrats on level 5", lvl5)
                         #move the email to processed
                        move_email_to_process(num)

                    elif parse_email_body(msg_content, lvl6key) is True:
                        print "found the solution to lvl6"
                        db['accounts'].update(dict(id=userid, level6="complete"), ['id'])
                        send_email(email_from, "Congrats on level 6", lvl6)
                         #move the email to processed
                        move_email_to_process(num)

                    elif parse_email_body(msg_content, lvl7key) is True:
			print "found the solution to lvl7"
                        db['accounts'].update(dict(id=userid, level7="complete"), ['id'])              
                        send_email(email_from,"Congrats you completed the hardest challenge", lvl7)
                        #move the email to processed
                        move_email_to_process(num)
                    elif email_from is not "admin@tigerscyberchallenge.net":
                        send_email(email_from,"tigers challenge auto-reply",  "Your email did not have a flag (or you did not put spaces around the flag). You are successfully registered for the challenge. If your email was an issue or a turn in for level 7 we will review it and get back to you. Thanks!")
                                                 #move the email to processed
                        move_email_to_toread(num)


            #if not a valid email
            else:
                #move the email to processed
                move_email_to_ignored(num)
		if (email_from_register):
                    send_email(email_from_register, "You are not worthy....", "Hopefully someday you are worthy... Most likely this is because you're not using a trusted email address. Please read the email for signing up again.")
		else:
		    print "moving random email"
		#elif (email.utils.parseaddr(msg['from'])[1]) is not "admin@tigerscyberchallenge.net":
                #    #this is a test
                #    send_email(email.utils.parseaddr(msg['from'])[1], "You are not worthy...", "Hopefully someday you are worthy... Most likely this is because you're not using a trusted email address. Please read the email for signing up again.")
    except Exception as e:
        print "caught an error processing the mail, will restart (hopefully)"
	print e
	exc_type, exc_obj, exc_tb = sys.exc_info()
    	fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    	print(exc_type, fname, exc_tb.tb_lineno)

#return the message information
def get_first_text_block(email_message_instance):

    try:
        maintype = email_message_instance.get_content_maintype()
        if maintype == 'multipart':
            for part in email_message_instance.get_payload():
                if part.get_content_maintype() == 'text':
                    return part.get_payload()
        elif maintype == 'text':
            return email_message_instance.get_payload()
    except:
        print "got an error getting the email message"

#Move the email then delete it
def move_email_to_process(msg_id):
    try:
        M.copy(msg_id, "Processed")
        M.store(msg_id, '+FLAGS', r'(\Deleted)')
        M.expunge()
    except:
        print "error moving the email"

#Move the email then delete it
def move_email_to_toread(msg_id):
    try:
        M.copy(msg_id, "ToRead")
        M.store(msg_id, '+FLAGS', r'(\Deleted)')
        M.expunge()
    except:
        print "error moving the email"

#Move the email then delete it
def move_email_to_ignored(msg_id):
    try:
        M.copy(msg_id, "Ignored")
        M.store(msg_id, '+FLAGS', r'(\Deleted)')
        M.expunge()
    except:
        print "error deleting the email"

#email parser -- add email addresses here
def parse_email_address(from_address):
    try:
        if from_address.endswith("us.af.mil"):
            return True
        elif from_address.endswith("bnxnet.com"):
            return True
        elif from_address.endswith("binaryx.net"):
	    return True
        #TODO: add parser that checks for personal emails that are registered
        else:
            return False
    except:
        print "error parsing the email addresses"

#parse the email given the body and a keyword
def parse_email_body(msg_body, keyword):

    if msg_body is None:
        print "recieved zero buffer in parse email"
        return False
    try:
	msg_body = ''.join(msg_body)
	print "parsing msg body : " + msg_body
        msg_content = msg_body.splitlines()

        for line in msg_content:
            if keyword.lower() in line.lower():
                return True

#        if re.search(keyword, text):
#	    return True
#        else:
#            return False

    except Exception as e:
        print "error parsing the email body"
	print e

#Send the email
def send_email(recipient, subject, body):

    try:

        gmail_user = EMAIL_ACCOUNT
        gmail_pwd = PASS
        FROM = EMAIL_ACCOUNT
        TO = recipient if type(recipient) is list else [recipient]
        SUBJECT = subject
        TEXT = body

        # Prepare actual message
        message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(gmail_user, gmail_pwd)
            server.sendmail(FROM, TO, message)
            server.close()
            print 'successfully sent the mail'
        except:
            print "failed to send mail"

    except:
        print "error sending email"


#check if email account exists
def check_account(email):
    print "Checking if account " + email + "exists"
    try:
        for account in db['accounts']:
            if (account['email'] == email):
                print "found the email account"
		print str(account['id']) + "found"
                return account['id']

        return False
    except:
        print "error in checking database for email"

def add_account(email_, team_):
    try:
        db['accounts'].insert(dict(email=email_, team=team_, level0 = "no",level01 = "no", level02 = "no",  level1 = "no", level2 = "no", level3 = "no", level4 = "no", level5 = "no", level6 = "no", level7 = "no"))
        print "added the new email"
    except:
        print "error adding account to database"

def update_website():

    try:

        users = create_list_users()
        teams = create_list_teams()

        #sort the list by highest number to lowest
        from operator import itemgetter
        #users = reversed(sorted(users,key=itemgetter(1)))
        #teams = reversed(sorted(teams))

        #print this to a website
        target = open("/var/www/html/scores.html", 'w')
        target.write("<html><head><style>table, th, td { border: 1px solid black; border-collapse: collapse;}th, td {padding: 5px;</style></head><body>")

        target.write("<b><center>Tigers Cyber Challenge 2015 Scores</center></b><br><br><br>")


        target.write("<table style=\"width:100%\"> <caption>Team Scores</caption")
        target.write("<tr><th>Team</th><th>Score</th></tr>")
        for item in teams:
            target.write("<tr>")
            for item2 in item:
                target.write("<td>" + str(item2) + "</td>")
            target.write("</tr>")
        target.write("</table>")


        target.write("<br><br>")

        target.write("<table style=\"width:100%\"><caption>Personal Scores</caption>")
        target.write("<tr><th>Email</th><th>Team</th><th>Score</th></tr>")
        for item in users:
            target.write("<tr>")
            for item2 in item:
                target.write("<td>" + str(item2) + "</td>")
            target.write("</tr>")
        target.write("</table>")

        target.write("<br><br>")

	#detail list of users
	target.write("<table style=\"width:100%\"><caption>Detailed Scores</caption>")
	target.write("<tr><th>Email</th><th>Team</th><th>Lvl 0</th><th>Lvl 0.1</th><th>Lvl 0.2</th><th>Lvl 1</th><th>Lvl 2</th><th>Lvl 3</th><th>Lvl 4</th><th>Lvl 5</th><th>Lvl 6</th><th>Lvl 7</th></tr>")
	for user in db['accounts']:
	    target.write("<tr>")
	    target.write("<td>" + user['email'] + "</td>")
	    target.write("<td>" + user['team'] + "</td>")
            if user['level0'] == "complete":
                target.write('<td bgcolor="#00FF00">1</td>')
	    else:
		target.write("<td>0</td>")
            
	    if user['level01'] == "complete":
                target.write('<td bgcolor="#00FF00">1</td>')
	    else:
		target.write("<td>0</td>")

            if user['level02'] == "complete":
                target.write('<td bgcolor="#00FF00">1</td>')
	    else:
		target.write("<td>0</td>")

            if user['level1'] == "complete":
                target.write('<td bgcolor="#00FF00">2</td>')
	    else:
		target.write("<td>0</td>")

            if user['level2'] == "complete":
                target.write('<td bgcolor="#00FF00">3</td>')
	    else:
		target.write("<td>0</td>")

            if user['level3'] == "complete":
                target.write('<td bgcolor="#00FF00">4</td>')
	    else:
		target.write("<td>0</td>")

            if user['level4'] == "complete":
                target.write('<td bgcolor="#00FF00">5</td>')
	    else:
		target.write("<td>0</td>")

            if user['level5'] == "complete":
                target.write('<td bgcolor="#00FF00">6</td>')
	    else:
		target.write("<td>0</td>")

            if user['level6'] == "complete":
                target.write('<td bgcolor="#00FF00">7</td>')
	    else:
		target.write("<td>0</td>")

            if user['level7'] == "complete":
                target.write('<td bgcolor="#00FF00">8</td>')
	    else:
		target.write("<td>0</td>")

	    target.write("</tr>")
	target.write("</table>")
	target.write("<br><br>")
        target.write("Last updated: " + time.strftime("%Y-%m-%d %H:%M"))
	target.close()

    except Exception as e:
	print "error updating website"
	print e

def create_list_users():
    try:
        users = []
        for user in db['accounts']:
            points = 0
            if user['level0'] == "complete":
                points = points + 1
	    if user['level01'] == "complete":
		points = points + 1
	    if user['level02'] == "complete":
		points = points + 1
            if user['level1']  == "complete":
                points = points + 2
            if user['level2']  == "complete":
                points = points + 3
            if user['level3']  == "complete":
                points = points + 4
            if user['level4']  == "complete":
                points = points + 5
            if user['level5']  == "complete":
                points = points + 6
	    if user['level6'] == "complete":
		points = points + 7
	    if user['level7'] == "complete":
		points = points + 8

            tup1 = (user['email'], user['team'], points)
            users.append(tup1)

        return sorted(users, key = lambda x: x[2], reverse=True)
    except:
        print "error creating list of users"

#create a list of teams for scoring
def create_list_teams():
    try:
        teams = []
        for user in db['accounts']:

            points = 0
            if user['level0'] == "complete":
                points = points + 1
	    if user['level01'] == "complete":
		points = points + 1
	    if user['level02'] == "complete":
		points = points + 1
            if user['level1']  == "complete":
                points = points + 2
            if user['level2']  == "complete":
                points = points + 3
            if user['level3']  == "complete":
                points = points + 4
            if user['level4']  == "complete":
                points = points + 5
            if user['level5']  == "complete":
                points = points + 6
	    if user['level6'] == "complete":
		points = points + 7
	    if user['level7'] == "complete":
		points = points + 8

            found_team = False
	    index = -1

	    if user['team'] == "noteam":
		found_team = True
	    else:
            #check if team already exists, if not...
                for item in teams:
		    index = index + 1
                    if user['team'] in item:
                        itemlst = list(item)
                        itemlst[1] = itemlst[1] + points
                        item = tuple(itemlst)
                        found_team = True
		        teams[index] = item

                    #print "test : " + item

                    #http://stackoverflow.com/questions/11458239/python-changing-value-in-a-tuple
            if not found_team:
                teams.append([user['team'], points])


	#sort teams
	

        return sorted(teams, key=lambda x: x[1], reverse=True)

    except Exception as e:
        print "error processing teams: "
	print e

#the registration note
reg_note = "\n\nRead the rules at http://tigerscyberchallenge.net/rules.html!\n Scores at http://tigerscyberchallenge.net/scores.html\n No scanning (nmap/etc), no hacking the scoring system, no DDOS style attacks! It's way easier than that.\n Now to get started... \n\n"
#Update these for instructions on each level
lvl0 = "Congrats - you got lvl0!"
lvl01 = "Congrats - you got lvl0.1!"
lvl02 = "Congrats - you got lvl0.2!"
lvl1 = "Congrats - you got lvl1!"
lvl2 = "Congrats - you got lvl2!"
lvl3 = "Congrats - you got lvl3!"
lvl4 = "Congrats - you got lvl4!"
lvl5 = "Congrats - you got lvl5!"
lvl6 = "Congrats - you got lvl6!"
lvl7 = "Congrats - you got lvl7!"

#the keys for each level
lvl0key = "miketys0nlovestigers"
lvl01key = "cyfer"
lvl02key = "ThereIsNoOffSwitchOnATiger"
lvl1key = "christian_bale"
lvl2key = "thisdoesntlooklikeatiger!"
lvl3key = "Tigers_are_great!_They_can't_be_beat!_If_I_was_a_tiger,_that_would_be_neat!"
lvl4key = "I'vegotalottabouncin'todo!"
lvl5key = "tigers_always_beat_pythons"
lvl6key = "72be8dee7c83a85c8e47746f6e62a947"
lvl7key = "most_13373s7_tiger"


if __name__ == "__main__":
    main()


'''

table - 'accounts'

email - team - level0 - level1 - level2 - level3 - level4 - level5

https://dataset.readthedocs.org/en/latest/quickstart.html

TODO:
Add exception handling
Add timer

                                                                         

'''
