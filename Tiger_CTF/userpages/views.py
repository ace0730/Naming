from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render,redirect
from forms import Register

def registration_view(request):
    if request.method == "POST":
        form = Register(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = Register()
    context = {
            'form': form,
            'title':'Registration Page',
            }
    return render(request,'userpages/register.html', context)


# Create your views here.
