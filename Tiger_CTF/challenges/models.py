from django.db import models
from django.utils.html import format_html
from django.contrib.auth.models import User
# This is the database model for the CTF Challenges
class Challenge(models.Model):
    number = models.IntegerField(unique=True)
    description = models.TextField()
    hint = models.TextField()
    key = models.CharField(max_length=128)
    points = models.IntegerField()

    #This method adds challenge to the integer field of Challenge and 
    #allows you to order the models on the next line after the method
    def format_number(self):
        return format_html('Challenge {}',
                            self.number)
    number.admin_order_field = 'number'
    #required for displaying an printable item
    def __unicode__(self):
        return format_html('Challenge{}', self.number)


class ChallengeFile(models.Model):
    challenge = models.ForeignKey('Challenge',)
    fi = models.FileField()

    def __unicode__(self):
        return format_html('Challenge {} -- File: {}', self.challenge, self.fi) 

class UserScore(models.Model):
    user = models.ForeignKey(User)
    challenge = models.ForeignKey('Challenge')
    def __unicode__(self):
        return format_html('{} -- {}', self.user, self.challenge) 

