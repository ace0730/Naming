from django import forms
from .models import Challenge, UserScore
from django.forms.util import ErrorList

class AnswerForm(forms.Form):
    answer = forms.CharField(label='Answer',max_length=100)

    def check_answer(self, chal, loged_user):
        ans = self.cleaned_data["answer"]
        if chal.key == ans:
            score = UserScore(user=loged_user, challenge=chal)
            score.save(force_insert=True)
            return True
        else:
            errors = self._errors.setdefault("answer",ErrorList())
            errors.append(u"That key was incorrect")
            return False

