from django.conf.urls import include, url
import views

urlpatterns = [
        url(r'^$', views.challenges, name='challenges'), 
        url(r'^(?P<challenge_number>[0-9]+)$', views.challenge, name='challenge'),
        ]
