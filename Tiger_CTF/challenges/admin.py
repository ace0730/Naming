from django.contrib import admin
from .models import Challenge, ChallengeFile, UserScore
#register the models for the admin page

#@admin.register(ChallengeFile,)
class ChallengeFileInline(admin.TabularInline):
    model = ChallengeFile
    fields = ('fi','challenge',)
    list_display = ('File','challenge',)
    #grab the name from the foreign key / challenge


@admin.register(Challenge,)
class ChallengeAdmin(admin.ModelAdmin):
    #variable for that lists the admin items
    inlines = [
                ChallengeFileInline,
                ]
    list_display = ('format_number',)
    fields = ('number',
            'description',
            'hint',
            'key',
            'points',
            )

@admin.register(UserScore,)
class UserScoresAdmin(admin.ModelAdmin):
    pass

