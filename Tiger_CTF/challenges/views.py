from django.http import Http404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Challenge,ChallengeFile, UserScore
from .forms import AnswerForm

def check_solved(user):
    challist = Challenge.objects.all()
    solved_list = [UserScore.objects.filter(challenge=x,user=user).exists() for x in challist]
    return zip(challist, solved_list)


@login_required
def challenges(request):
    challist = check_solved(request.user)
    return render(request,'challenges/challenges-base.html',{'challist':challist})

@login_required
def challenge(request, challenge_number):
    c_complete = ''
    challist = check_solved(request.user) 
    challenge = Challenge.objects.get(number=challenge_number)
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            ans = form.cleaned_data
            if(form.check_answer(challenge, request.user)):
                c_complete = 'Congratulations, That is the correct key!'
            return render(request,'challenges/challenge.html',
                    {'challist':challist,
                     'challenge':challenge,
                     'form':form,
                     'c_complete':c_complete,
                     })
    else:
        form = AnswerForm()
        if (UserScore.objects.filter(challenge=challenge,user=request.user).exists()):
            c_complete = 'You have already completed this challenge!'

    return render(request,'challenges/challenge.html',
            {'challist':challist,
                'challenge':challenge,
                'form':form,
                'c_complete':c_complete,
                })


# Create your views here.
