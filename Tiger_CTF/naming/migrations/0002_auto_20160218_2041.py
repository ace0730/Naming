# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('naming', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='namee',
            name='rank',
            field=models.CharField(max_length=4, choices=[(b'SSgt', b'Staff Sergeant'), (b'TSgt', b'Technical Sergeant'), (b'Capt', b'Captain'), (b'Maj', b'Major')]),
        ),
    ]
