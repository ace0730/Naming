from django import forms

class VoteForm(forms.Form):
    vote = forms.IntegerField(max_value=100)
