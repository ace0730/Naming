from django.db import models
from django.utils.html import format_html
from django.contrib.auth.models import User

class Namee(models.Model):
    RANK = [
        ('SSgt', 'Staff Sergeant'),
        ('TSgt', 'Technical Sergeant'),
        ('Capt', 'Captain'),
        ('Maj', 'Major'),
            ]
    rank = models.CharField(max_length=4, choices = RANK)
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    enabled = models.BooleanField(default=False)

    def __unicode__(self):
        return format_html('{} {}'.format(self.rank,
                                              self.last_name,
                                              )
                            )
class NickName(models.Model):
    namee = models.ForeignKey('Namee',
                       on_delete=models.CASCADE)
    nick = models.CharField(max_length=32)

    def __unicode__(self):
        return format_html('{} {} \"{}\" {}'.format(self.namee.rank, self.namee.first_name ,self.nick, self.namee.last_name))

class Vote(models.Model):
    nick_name = models.ForeignKey('NickName',
                      on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __unicode__(self):
        return format_html('{} - {}'.format(self.user, self.nick_name))
# Create your models here.
