from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from .models import Namee, NickName, Vote
from django.contrib.auth.models import User
from .forms import VoteForm
from operator import itemgetter

def progress(nick_name):
    total_votes = float(Vote.objects.filter(nick_name=nick_name).count())
    perc_complete = 0
    if total_votes:
        total_users = float(User.objects.all().count()) - 2
        if total_users:
            perc_complete = int(total_votes/(total_users)*100)
            print 'Perc Complete:{}/{} {}'.format(total_users,total_votes,perc_complete)
    return perc_complete


@login_required
def naming(request):
    namees = Namee.objects.all()
    perc_complete = 0
    namees_disabled = Namee.objects.filter(enabled=False)
    nicks_count = NickName.objects.filter(namee=namees_disabled).annotate(vote_count=Count('vote'))
    set_nicks = [nicks_count.filter(
                namee=x,
                vote_count__gt=0
                ).order_by(
                    '-vote_count'
                    ).first() for x in namees_disabled]

    enabled_name = ''
    nick_votes= ''
    if namees.filter(enabled=True).exists():
        enabled_name = namees.filter(enabled=True)[0]
        nick_name = NickName.objects.filter(namee=enabled_name)
        nick_votes = [(x,Vote.objects.filter(nick_name__id=x.id).count()) for x in nick_name] 
        perc_complete = progress(nick_name)



    return render(request,
                  'naming/naming.html',
                  {'enabled_name':enabled_name, 
                   'namees':namees, 
                   'nick_votes':nick_votes, 
                   'set_nicks':set_nicks,
                   'perc_complete':perc_complete,
                   })

def sort_scores(nick_names):
            nick_names = [(x,Vote.objects.filter(nick_name__id=x.id).count()) for x in nick_names] 
            nick_names = sorted(nick_names, key=itemgetter(1))[::-1]
            nick_1 = nick_names[0]
            nick_2 = nick_names[1:4]
            nick_3 = nick_names[4:]
            return (nick_1,nick_2,nick_3)



@login_required
def vote(request, namee_id):
    namees = Namee.objects.all();
    namee = Namee.objects.get(id=namee_id)
    nick_names = NickName.objects.filter(namee=namee)
    perc_complete = progress(nick_names)
    if request.method == 'POST':
        form = VoteForm(request.POST)
        if form.is_valid():
            #vulnerable!!!!
            vote = form.cleaned_data['vote']
            nick = NickName.objects.get(id=vote)
            old_v = Vote.objects.filter(user=request.user, nick_name__namee=namee)
            old_v.delete()
            v = Vote(user=request.user,nick_name=nick)
            v.save()
            (nick_1,nick_2,nick_3) = sort_scores(nick_names)
            return render(request,'naming/votes.html',{ 
                                                       'namees':namees, 
                                                       'namee':namee,
                                                       'nick_1':nick_1,
                                                       'nick_2':nick_2,
                                                       'nick_3':nick_3,
                                                       'perc_complete':perc_complete,
                                                       })
        pass
    else:
        (nick_1,nick_2,nick_3) = sort_scores(nick_names)
        form = VoteForm()
        return render(request,'naming/votes.html',{
                                                    'namees':namees, 
                                                    'namee':namee,
                                                    'nick_1':nick_1,
                                                    'nick_2':nick_2,
                                                    'nick_3':nick_3,
                                                    'perc_complete':perc_complete,
                                                    })


@login_required
def score(request, namee_id):
    pass

# Create your views here.
