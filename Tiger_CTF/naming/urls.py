from django.conf.urls import include, url
from .views import naming, vote, score

urlpatterns = [
        url(r'^$', naming, name = 'naming'),
        url(r'^vote/([0-9]+)$', vote, name='vote'),
        url(r'^score/([0-9]+)$', vote, name='score'),
        ]
