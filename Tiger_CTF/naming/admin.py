from django.contrib import admin
from .models import Namee, NickName, Vote

class NickNameInline(admin.TabularInline):
    model = NickName

def mark_enabled(modeladmin, request, queryset):
    queryset.update(enabled=True)
    mark_enabled.short_description = 'Enable marked namees'

def mark_disabled(modeladmin, request, queryset):
    queryset.update(enabled=False)
    mark_disabled.short_description = 'Disable marked namees'

@admin.register(Namee,)
class NameeAdmin(admin.ModelAdmin):
    list_display = ('last_name','first_name','enabled')
    inlines = [
            NickNameInline,
            ]
    actions = [mark_enabled,mark_disabled]

@admin.register(Vote,)
class VoteAdmin(admin.ModelAdmin):
    pass


# Register your models here.
